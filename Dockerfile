#Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

#Distroless runtime stage
FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/ids721week4 /app/

#Use non-root user
USER nonroot:nonroot

#Set up app directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

#Expose port
EXPOSE 8080

#Run the app
ENTRYPOINT ["/app/ids721week4"]
