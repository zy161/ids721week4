# IDS721Week4

## Develop Actix Web Service
1. Use `cargo new ids721week4` to create a new binary-based Cargo project
2. Add required dependencies for this project in `Cargo.toml`
```toml
[dependencies]
actix-web = "4"
serde = { version = "1.0.196", features = ["derive"]}
```
3. Start a web server following the instructions on [Getting Started](https://actix.rs/docs/getting-started)
4. Create greedy coin change algorithm in the new created file `lib.rs` under `src`
5. update the `main.rs` to import the algorithm iin `lib.rs`
`use ids721week4::greedy_coin_change;`
6. Start the web service with `cargo run` and test the function with `cargo test`
- <img src="img/cargotest.png" style="width:400px;">
7. Go to `localhost:8080` and test the web service
- <img src="img/webapplication.png" style="width:400px;">

## Containerize Actix Web Service
1. Create `Dockerfile` in the application's root directory
2. Build the Docker image with `docker build -t idsweek4webapp .`

`Note`: Unlike Axum, Actix requires rust's version to be 1.68 or later, make sure we are using `FROM rust:1.68 AS build` in the Dockerfile

## Docker Image and Local Run
1. Start a Docker container with `docker run -p 8080:8080 idsweek4webapp`
- <img src="img/runningdocker.png" style="width:400px;">
2. Go to `localhost:8080` and test the web service
- <img src="img/runningcontainerweb.png" style="width:400px;">
